# Club-Discovery


1 Versión del compilador utilizada
Hallazgo:
La versión del compilador utilizada es denotada de la siguiente forma ^x.y.z.
Evidencia:

Implicación:
Al utilizar la denotación ^x.y.z. se indica que el programa se puede compilar con la versión indicada y superiores, lo que implica que esas futuras versiones pueden manejar construcciones del lenguaje que no fueron consideradas en el momento del desarrollo del código.
Recomendación:
Se recomienda especificar la versión exacta del compilador a utilizar, de la forma pragma solidity x.y.z

2 Utilización del puerto por defecto

Hallazgo:
El número del puerto especificado en el archivo de configuración es incorrecto.
Evidencia:

Implicación:
Al utilizar un puerto diferente al recomendado, se presentan errores de compilación mientras se realizan pruebas.
Recomendación:
Es recomendable indicar como número de puerto el 8545 mientras se esté en la etapa de pruebas o desarrollo para evitar problemas de compilación.  

3 Función declarada como constant

Hallazgo:
El estado de la función es definida como “constant” y no como “view”.
Evidencia:

Archivos
Líneas
Garantor.sol
8,9







Implicación:
Está previsto que el uso de “view” en las funciones se deje de utilizar en la versión 0.5.0, para denotar que el estado de la función no será modificado.
Ver: http://solidity.readthedocs.io/en/latest/contracts.html#constant-state-variables
Recomendación:
Para declarar que el estado de una función no será modificado, se recomienda definir el estado de la misma como “view” y no como “constant”.


4 Nivel de visibilidad implícito

Hallazgo:
Variable sin nivel de visibilidad definido.
Evidencia:

Archivos
Líneas
Garantor.sol
19







Implicación:
El nivel de visbilidad en Solidity por defecto es “public”, pero para evitar cualquier confusión, se recomienda definir el mismo, ya sea “public”, “internal”, “private” o “external”.
Recomendación:
Se debe evitar la ambigüedad en la declaración de niveles de visibilidad. Ver  http://solidity.readthedocs.io/en/develop/contracts.html#visibility-and-getters.

5 Riesgos de Over and Under Flows 

Hallazgo:
Se encontraron funciones que 
Evidencia:


Archivos
Líneas
Garantor.sol
140,142,144







Implicación:
Existe el  el riesgo, muy bajo, en el que Solidity es propenso a over o under flows de enteros. 
Recomendación:
Para las operaciones matemáticas, es recomendable hacer uso de la librería “SafeMath,sol” y utilizar las funciones .mul(), .add(), .sub() y .div() en caso de ser necesario.





